CREATE SCHEMA crawler;
USE crawler;

CREATE TABLE website(
	id_website INT PRIMARY KEY AUTO_INCREMENT,
    dominio VARCHAR(500) NOT NULL,
    ip VARCHAR(90) NOT NULL
);

CREATE TABLE pagina(
	id_pagina INT PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(300),
    imagem VARCHAR(512),
    id_website INT NOT NULL,
    url VARCHAR(6500) NOT NULL,
    referer VARCHAR(6500) NOT NULL,
    ultima_visita DATETIME,
    ultimo_estado INT,
    FOREIGN KEY (id_website) REFERENCES website(id_website)
);

CREATE TABLE `utilizador` (
  `id_utilizador` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(256) NOT NULL,
  `token` varchar(255) NOT NULL,
  `data_token` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `utilizador`
-- password: 123
INSERT INTO `utilizador` (`id_utilizador`, `email`, `password`, `token`, `data_token`) VALUES
(1, 'admin@crawler.com', '$2y$10$5Vxg4Nm2fV2bA.cMKl0U8Oyq9IAdpLPmoYVYPHiscDg9oDpcu9gS6', '  ', '2019-04-10 00:00:00');