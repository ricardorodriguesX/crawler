<?php
/*
    require_once __DIR__."/../dao/UtilizadorDAO.php";
    require_once __DIR__."/../dao/ClienteDAO.php";
*/
    require_once __DIR__."/../BackOffice/dto/BaseDTO.php";
    foreach(glob(dirname(__DIR__) . '/../BackOffice/dao/*.php') as $class_path ){
        require_once ''.$class_path.'' ;
    }


    class Validator {

        //METODOS
        //VALIDACAO PREENCHIDO
        public static function validaPreenchido($valor) {
            $baseDTO = new BaseDTO();

            if (!isset($valor) || empty($valor)) {
                $baseDTO->setNumeroTransacao(-1);
                $baseDTO->setDescricaoTransacao("Por favor preencha os seguintes campos");
            } else {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            }

            return $baseDTO;
        }
        //VALIDACAO EMAIL
        public static function validaEmail($email) {
            $baseDTO = new BaseDTO();

            if (Validator::validaPreenchido($email)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-10);
                $baseDTO->setDescricaoTransacao("O email não está preenchido");
            } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $baseDTO->setNumeroTransacao(-30);
                $baseDTO->setDescricaoTransacao("O formato do email está inválido, exemplo: email@isp.pt");
            } else {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            }

            return $baseDTO;
        }
        //validar autenticacao
        public static function validarLogin($email, $password ){
            $baseDTO = new BaseDTO();

            if (Validator::validaEmail($email)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-10);
                $baseDTO->setDescricaoTransacao("O email não está preenchido");
            } else {
                $utilizadorDAO = new UtilizadorDAO();
                //OBTER USER
                $utilizador = $utilizadorDAO->consultar(null,$email);

                if(!isset($utilizador[0]["id_utilizador"])) {
                    $baseDTO->setNumeroTransacao(-80);
                    $baseDTO->setDescricaoTransacao("O utilizador não existe");
                }elseif (!password_verify($password,$utilizador[0]["password"])) {
                    $baseDTO->setNumeroTransacao(-180);
                    $baseDTO->setDescricaoTransacao("O utilizador inválido");
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }

            return $baseDTO;
        }
        //comparar passwords
       public static function compararPassword($hash, $password)
        {
            $baseDTO = new BaseDTO();
            if (crypt($password, $hash) == $hash) {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            } else {
                $baseDTO->setNumeroTransacao(-200);
                $baseDTO->setDescricaoTransacao("A password errada");
            }
            return $baseDTO;
        }
        //VALIDACAO DE PASSWORD
        public static function validaPassword($password) {
            $baseDTO = new BaseDTO();

            if (!isset($password) || empty($password)) {
                $baseDTO->setNumeroTransacao(-50);
                $baseDTO->setDescricaoTransacao("A password não está preenchida");
            } else
                if (strlen($password) >= 8) {
                    if (preg_match('#[a-z]+#', $password)) {
                        if (preg_match('#[A-Z]+#', $password)) {
                            if (preg_match('#[0-9]+#', $password)) {
                                $baseDTO->setNumeroTransacao(0);
                                $baseDTO->setDescricaoTransacao("Sucesso");
                            } else {
                                $baseDTO->setNumeroTransacao(-51);
                                $baseDTO->setDescricaoTransacao("A password deve conternúmeros, letras minúsculas e letras maiusculas");
                            } //preg_match('#[0-9]+#', $this->getPassword())
                        } else {
                            $baseDTO->setNumeroTransacao(-52);
                            $baseDTO->setDescricaoTransacao("A password deve conter números, letras minúsculas e letras maiusculas");
                        }//preg_match('#[A-Z]+#',$this->getPassword())
                    } else {
                        $baseDTO->setNumeroTransacao(-53);
                        $baseDTO->setDescricaoTransacao("A password deve conter números, letras minúsculas e letras maiusculas");
                    }//preg_match('#[a-z]+#', $this->getPassword())
                } else {
                    $baseDTO->setNumeroTransacao(-55);
                    $baseDTO->setDescricaoTransacao("A password deve conter pelo menos 8 caracteres");
                }//strlen($this->getPassword())>=8

            return $baseDTO;
        }

        //VALIDACAO DE PASSWORDS
        public static function validaPasswords($password, $confirmacaoPassword) {
            $baseDTO = new BaseDTO();

            if (Validator::validaPassword($password)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-50);
                $baseDTO->setDescricaoTransacao("A password não está preenchida");
            } elseif (Validator::validaPassword($confirmacaoPassword)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-150);
                $baseDTO->setDescricaoTransacao("A confirmação da password não está preenchida");
            } elseif ($password != $confirmacaoPassword) {
                $baseDTO->setNumeroTransacao(-160);
                $baseDTO->setDescricaoTransacao("A password e a confirmação de password não coincidem");
            } else {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            }

            return $baseDTO;
        }

        //VALIDACAO DE NOME
        public static function validaNome($nome) {
            //INSTANCIA dto
            $baseDTO = new BaseDTO();

            if (Validator::validaPreenchido($nome)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-120);
                $baseDTO->setDescricaoTransacao("O nome não está preenchido");

                return $baseDTO;
            } else if (preg_match('#\d#', $nome)) {
                $baseDTO->setNumeroTransacao(-130);
                $baseDTO->setDescricaoTransacao("O nome só pode conter letras");

                return $baseDTO;
            } else {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");

                return $baseDTO;
            }
        }

        //VALIDACAO DE TEXTO
        public static function validaDescricao($descricao) {
            //INSTANCIA dto
            $baseDTO = new BaseDTO();

            if (Validator::validaPreenchido($descricao)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-170);
                $baseDTO->setDescricaoTransacao("A descrição não está preenchida");

                return $baseDTO;
            } elseif (strlen($descricao) > 800) {
                $baseDTO->setNumeroTransacao(-180);
                $baseDTO->setDescricaoTransacao("A descrição ultrapassa o limite de caracteres");

                return $baseDTO;
            } else {
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");

                return $baseDTO;
            }
        }

        //VALIDACAO DE NUMERO
        public static function validaNumero($numero, $tamanho ) {
            //INSTANCIA dto
            $baseDTO = new BaseDTO();

            if (!is_numeric($numero)) {
                $baseDTO->setNumeroTransacao(-290);
                $baseDTO->setDescricaoTransacao("Número Inválido");
            } elseif (strlen($numero) != $tamanho) {
                $baseDTO->setNumeroTransacao(-299);
                $baseDTO->setDescricaoTransacao("Tem de ter" . $tamanho . " carateres");
            }else{
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            }

            return $baseDTO;
        }

        //VALIDACAO DE FLAGS
        public static function validaFlags($flag) {
            //INSTANCIA dto
            $baseDTO = new BaseDTO();

            if (Validator::validaPreenchido($flag)->getNumeroTransacao() < 0) {
                $baseDTO->setNumeroTransacao(-380);
                $baseDTO->setDescricaoTransacao("A flag não é válida");
            } else {
                //ARRAY DE FLAGS POSSIVEIS
                $encontrou = false;
                $flagsPossiveis = array(
                    "S", "N"
                );

                //PARA CADA FLAG POSSIVEL
                foreach ($flagsPossiveis as $flagsPossivel) {
                    if ($flag == $flagsPossivel) {
                        $encontrou = true;
                    }
                }

                if (!$encontrou) {
                    $baseDTO->setNumeroTransacao(-380);
                    $baseDTO->setDescricaoTransacao("A flag não é válida");
                } else {
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }

            return $baseDTO;
        }

        //validar sessao
        public static function validarSessao($email,$token){
            //baseDTO
            $baseDTO = new BaseDTO();
            $utilizadorDAO = new UtilizadorDAO();
            //OBTER USER
            $utilizador = $utilizadorDAO->consultar(null ,$email,null,$token);

            if(Validator::validaEmail($email)->getNumeroTransacao()  < 0){
                $baseDTO->setNumeroTransacao(-90);
                $baseDTO->setDescricaoTransacao("A sua sessão expirou");
            }elseif(Validator::validaPreenchido($token)->getNumeroTransacao() <0){
                $baseDTO->setNumeroTransacao(-90);
                $baseDTO->setDescricaoTransacao("A sua sessão expirou");
            }elseif(!isset($utilizador[0]["email"])){
                $baseDTO->setNumeroTransacao(-90);
                $baseDTO->setDescricaoTransacao("A sua sessão expirou");
            }else{
                $data_now=new DateTime();
                $data_token=DateTime::createFromFormat('Y-m-d H:i:s',$utilizador[0]["data_token"]);
                $diff=$data_now->diff($data_token)->h;
                if ($diff >= 3){
                    //Criar token
                    $baseDTO->setNumeroTransacao(-90);
                    $baseDTO->setDescricaoTransacao("A sua sessão expirou");

                }else {

                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;
        }

        //verificar se é double
        public static function validarDouble($numero){
            //baseDTO
            $baseDTO = new BaseDTO();

            if (Validator::validaPreenchido($numero)->getNumeroTransacao() <0){
                $baseDTO->setNumeroTransacao(-18);
                $baseDTO->setDescricaoTransacao("O valor não está preenchido");
            }elseif(!doubleval($numero)){
                $baseDTO->setNumeroTransacao(-28);
                $baseDTO->setDescricaoTransacao("Valor mal preenchido.Exemplo '10.4'");
            }else{
                $baseDTO->setNumeroTransacao(0);
                $baseDTO->setDescricaoTransacao("Sucesso");
            }
            return $baseDTO;
        }


        //validar Se existe User
        public static function validarSeExisteUser($email){
            //baseDTO
            $baseDTO = new BaseDTO();
            $utilizadorDAO = new UtilizadorDAO();
            //OBTER USER
            $utilizadores = $utilizadorDAO->consultar();
            //REMOVE ULTIMA POSICAO ARRAY
            unset($utilizadores[sizeof($utilizadores)-1]);

            foreach ($utilizadores as $utilizador){
                if($utilizador['email'] == $email){
                    $baseDTO->setNumeroTransacao(-38);
                    $baseDTO->setDescricaoTransacao("Este utilizador já existe");
                    return $baseDTO;
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;

        }

        //validar Se existe User
        public static function validarSeExisteCliente($nome){
            //baseDTO
            $baseDTO = new BaseDTO();
            $clienteDAO = new ClienteDAO();
            //OBTER USER
            $clientes = $clienteDAO->consultar();
            //REMOVE ULTIMA POSICAO ARRAY
            unset($clientes[sizeof($clientes)-1]);

            foreach ($clientes as $cliente){
                if($cliente['nome'] == $nome){
                    $baseDTO->setNumeroTransacao(-38);
                    $baseDTO->setDescricaoTransacao("Este cliente já existe");
                    return $baseDTO;
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;

        }

        //validar Se existe User sem ser o prorpio
        public static function validaSeExisteDifID($classe,$coluna,$id,$var){
            //baseDTO
            $baseDTO = new BaseDTO();
            $classeDAO = new $classe();
            //OBTER
            $lista = $classeDAO->consultar();
            //REMOVE ULTIMA POSICAO ARRAY
            unset($lista[sizeof($lista)-1]);

            foreach ($lista as $linha){
                if($linha[$coluna] == $var && $linha['id']!=$id ){
                    $baseDTO->setNumeroTransacao(-38);
                    $baseDTO->setDescricaoTransacao("Este registo já existe!");
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;
        }



        //validar Se existe Cliente sem ser o prorpio
        public static function validaNomeDifID($id,$nome){
            //baseDTO
            $baseDTO = new BaseDTO();
            $clienteDAO = new ClienteDAO();
            //OBTER USER
            $clientes = $clienteDAO->consultar();
            //REMOVE ULTIMA POSICAO ARRAY
            unset($clientes[sizeof($clientes)-1]);

            foreach ($clientes as $cliente){
                if($cliente['nome'] == $nome && $cliente['id'] != $id){
                    $baseDTO->setNumeroTransacao(-38);
                    $baseDTO->setDescricaoTransacao("Este cliente já existe");
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;

        }
        //validar nif
       public static function validaNIF($nif, $ignoreFirst=true) {
            $baseDTO = new BaseDTO();
            //Limpamos eventuais espaços a mais
            $nif=trim($nif);
            //Verificamos se é numérico e tem comprimento 9
            if (!is_numeric($nif) || strlen($nif)!=9) {
                $baseDTO->setNumeroTransacao(-299);
                $baseDTO->setDescricaoTransacao("Tem de ter 9 carateres");
            } else {
                $nifSplit=str_split($nif);
                //O primeiro digíto tem de ser 1, 2, 5, 6, 8 ou 9
                //Ou não, se optarmos por ignorar esta "regra"
                if (
                    in_array($nifSplit[0], array(1, 2, 5, 6, 7, 8, 9))
                    ||
                    $ignoreFirst
                ) {
                    //Calculamos o dígito de controlo
                    $checkDigit=0;
                    for($i=0; $i<8; $i++) {
                        $checkDigit+=$nifSplit[$i]*(10-$i-1);
                    }
                    $checkDigit=11-($checkDigit % 11);
                    //Se der 10 então o dígito de controlo tem de ser 0
                    if($checkDigit>=10) $checkDigit=0;
                    //Comparamos com o último dígito
                    if ($checkDigit==$nifSplit[8]) {
                        $baseDTO->setNumeroTransacao(0);
                        $baseDTO->setDescricaoTransacao("Sucesso");
                    } else {
                        $baseDTO->setNumeroTransacao(-298);
                        $baseDTO->setDescricaoTransacao("Nif invalido");
                    }
                } else {
                    $baseDTO->setNumeroTransacao(-298);
                    $baseDTO->setDescricaoTransacao("Nif invalido");
                }
            }
            return $baseDTO;
        }
		 //validar Se existe
        public static function validaSeExiste($classe,$coluna,$var){
            //baseDTO
            $baseDTO = new BaseDTO();
            $classeDAO = new $classe();
            //OBTER
            $lista = $classeDAO->consultar();
            //REMOVE ULTIMA POSICAO ARRAY
            unset($lista[sizeof($lista)-1]);

            foreach ($lista as $linha){
                if($linha[$coluna] == $var ){
                    $baseDTO->setNumeroTransacao(-38);
                    $baseDTO->setDescricaoTransacao("Este registo já existe!");
                }else{
                    $baseDTO->setNumeroTransacao(0);
                    $baseDTO->setDescricaoTransacao("Sucesso");
                }
            }
            return $baseDTO;
        }


    }
