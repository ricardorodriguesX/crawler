<?php

require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/CrawlerDAO.php';

class Crawler {

    //ATRIBUTOS
    private $lista;
    private $copy;

    //ACESSORES
    public function getLista() {
        return $this->lista;
    }

    public function setLista($lista) {
        $this->lista = $lista;
    }

    //CONSTRUTOR
    public function __construct($lista) {
        $this->lista = $lista;
        $this->copy = $lista;
    }

    //METODOS
    public function crawler() {
        if (sizeof($this->lista) > 0) {
            $itemLista = $this->lista[0];
            $url = $itemLista["link"];
 
            echo "\n\nA validar URL $url";
            if ($this->validateUrl($url)) {
                echo "\n\nA iniciar crawl em $url";
                
                //REMOVE URL DA LISTA
                $this->removeUrlLista($url);

                echo "\n\nCrawl iniciado em $url";
                echo "\n\nA obter página $url";
                
                //OBTEM PAGINA DO LINK
                $pagina = $this->obtemPagina($url);
                
                echo "\n\nPágina $url obtida";
                
                echo "\n\nA obter links da página $url";
                //OBTEM LINKS
                $links = $this->obtemLinks($pagina);
                
                echo "\n\nLinks da página $url obtidos";
                
                echo "\n\nA inserir dados da página $url";
                //INSERE DADOS
                $this->insereDados($pagina,$itemLista);
                
                echo "\n\nDados da página $url inseridos";

                echo "\n\nA validar links da página $url";
                
                foreach ($links as $link) {
                    if ($this->validateUrl($link->getAttribute("href"))) {
                        $linkAux = $link->getAttribute("href");
                        $paginaAux = $this->obtemPagina($linkAux);
                        
                        //VALIDAR SE PAGINA PERTENCE A LISTA DE ENTRY POINTS
                        if (!$this->crawlRecente($paginaAux) && $this->dominioPertenceALista($paginaAux)) {
                            echo "\n\nLink $linkAux da página $url válido";
                            array_push($this->lista, array("link"=>$link->getAttribute("href"), "referer"=>$url));
                        }else{
                            echo "\n\nLink $linkAux da página $url não pertence à lista ou é demasiado recente";
                        }
                    }
                }

                echo "\n\nCrawl da página $url terminado";
                echo "\n\n--------------------------------------------------------------------------------------------";
                
                //CRAWLER
                $this->crawler();
            }
        }
    }

    //FUNCOES ADICIONAIS
    private function removeUrlLista($url) {
        for ($i = 0; $i < sizeof($this->lista); $i++) {
            if ($url == $this->lista[$i]["link"]) {
                array_splice($this->lista, $i, 1);
            }
        }
    }

    private function obtemPagina($url) {
        //INSTANCIA CURL
        $ch = curl_init();

        //OPCOES CURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //EXECUTA PEDIDO
        $pedido = curl_exec($ch);

        $pedido = $this->curlParaArray($pedido);

        $ip = curl_getinfo($ch, CURLINFO_PRIMARY_IP);

        $estado = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //OBTEM HTML
        $html = file_get_contents($url);

        //RETORNA DADOS
        return array(
            "head" => $pedido,
            "html" => $html,
            "estado" => $estado,
            "url" => $url,
            "ip" => $ip
        );
    }

    private function obtemLinks($pagina) {
        $links = array();

        try {
            if ($pagina["html"] != "") {
                //CARREGA HTML
                $dom = new IvoPetkov\HTML5DOMDocument();
                $dom->loadHTML($pagina["html"]);

                //OBTEM LINKS
                $links = $dom->querySelectorAll('a');
            } else {
                $links = array();
            }
        } catch (Exception $ex) {
            $links = array();
        }


        return $links;
    }

    private function dominioPertenceALista($pagina) {
        $existe = false;

        $dominio = parse_url($pagina["url"])["host"];
        $schema = parse_url($pagina["url"])["scheme"];

        foreach ($this->copy as $domainPrincipal) {
            if ($schema . "://" . $dominio == $domainPrincipal["link"]) {
                $existe = true;
                break;
            }
        }

        return $existe;
    }

    private function crawlRecente($pagina) {
        //INSTANCIA DAO
        $crawlerDAO = new CrawlerDAO();

        $pagina = $crawlerDAO->obtemPaginas(null, null, $pagina["url"]);

        if (isset($pagina[0]["id_pagina"])) {
            $dataHoje = new DateTime();
            $ultimaVisita = new DateTime($pagina[0]["ultima_visita"]);

            if ($ultimaVisita->diff($dataHoje)->d < 15) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function insereDados($pagina, $itemLista) {
        //INSTANCIA DAO
        $crawlerDAO = new CrawlerDAO();

        $dominio = parse_url($pagina["url"])["host"];

        $ip = $pagina["ip"];

        //OBTER WEBSITE
        $website = $crawlerDAO->obtemWebsites(null, $dominio, null);

        //INSERIR WEBSITE
        if (!isset($website[0]["id_website"])) {
            $idWebsite = $crawlerDAO->insereWebsite($dominio, $ip)["ultimoID"];
        } else {
            $idWebsite = $website[0]["id_website"];
        }

        $titulo = "";
        try {
            //CARREGA HTML
            $dom = new IvoPetkov\HTML5DOMDocument();
            $dom->loadHTML($pagina["html"]);

            $titulo = is_object($dom) ? $dom->querySelector('title')->innerHTML : "";
        } catch (Exception $ex) {
            $titulo = "";
        }

        //GUARDA IMAGEM
        $imagem = $this->guardaImagem($pagina["url"]);

        //REMOVE PAGINA
        $crawlerDAO->removePagina($pagina["url"]);

        //INSERIR PAGINA
        $crawlerDAO->inserePagina($titulo, $imagem, $idWebsite, $pagina["url"], date("Y-m-d H:i:s"), $pagina["estado"], $itemLista["referer"]);
    }

    private function validateUrl($url) {
        $pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";

        return preg_match($pattern, $url);
    }

    private function curlParaArray($headerContent) {
        $headers = array();


        $arrRequests = explode("\r\n\r\n", $headerContent);

        for ($index = 0; $index < count($arrRequests) - 1; $index++) {

            foreach (explode("\r\n", $arrRequests[$index]) as $i => $line) {
                if ($i === 0)
                    $headers[$index]['http_code'] = $line;
                else {
                    list ($key, $value) = explode(': ', $line);
                    $headers[$index][$key] = $value;
                }
            }
        }

        return $headers;
    }

    private function guardaImagem($url) {
        $nomeImg = "";
        try {

            $api_response = file_get_contents("https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=$url&screenshot=true");
            //decode json data
            $result = json_decode($api_response, true);
            //screenshot data
            $screenshot = $result['screenshot']['data'];
            $screenshot = str_replace(array('_', '-'), array('/', '+'), $screenshot);
            $img = str_replace('data:image/jpeg;base64,', '', $screenshot);
            $nomeImg = hash("sha1", $url) . ".jpg";
            file_put_contents('../Imagens/' . $nomeImg, base64_decode($img));
        } catch (Exception $ex) {
            $nomeImg = "";
        }


        return $nomeImg;
    }

}
