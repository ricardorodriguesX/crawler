<?php

require_once __DIR__ . "/../Utils/config.php";

class BaseDAO {
    //ATRIBUTOS

    private $BDnome;
    private $BDip;
    private $BDusername;
    private $BDpassword;
    private $BDligacao;



    //ACESSORES

    public function setBDnome($BDnome)
    {
        $this->BDnome = $BDnome;
    }

    public function setBDip($BDip)
    {
        $this->BDip = $BDip;
    }

    public function setBDusername($BDusername)
    {
        $this->BDusername = $BDusername;
    }

    public function setBDpassword($BDpassword)
    {
        $this->BDpassword = $BDpassword;
    }

    public function getBDligacao()
    {
        return $this->BDligacao;
    }
    //CONSTRUTOR
    function __construct()
    {

        $this->setBDnome(name);
        $this->setBDip(server);
        $this->setBDusername(user);
        $this->setBDpassword(password);    }

    //METODOS
    //METODO PARA PREPARAR A QUERY
    public function prepare($query)
    {
        $this->BDligacao = new mysqli(
            $this->BDip,
            $this->BDusername,
            $this->BDpassword,
            $this->BDnome
        );

        $this->BDligacao->set_charset(charset);

        return $this->BDligacao->prepare($query);
    }

    //METODO PARA INSERIR
    public function insert($query)
    {
        $query->execute();
        $id = $this->BDligacao->insert_id;
        $this->BDligacao->close();
        $array_transacao = array(
            "numeroTransacao" => $query->errno,
            "descricaoTransacao" => $query->error,
            "ultimoID" => $id
        );
        return $array_transacao;
    }

    //METODO PARA OBTER DADOS
    public function select($query)
    {
        // declarar array associativo de resultados
        $arrayResultados = array();
        // executar query
        $query->execute();

        // resultado
        $resultado = $query->get_result();

        while ($row = $resultado->fetch_assoc()) {
            // push para o arrayResultados
            array_push($arrayResultados, $row);
        }

        $query->free_result();

        // fechar ligacao
        $this->BDligacao->close();

        // adicionar chave de erros
        $arrayErros = array(
            "numeroTransacao" => $query->errno,
            "descricaoTransacao" => $query->error
        );

        // push dos erros para o arrayResultados
        array_push($arrayResultados, $arrayErros);

        // retornar array associativo
        return $arrayResultados;
    }

    //METODO PARA EDITAR
    public function update($query)
    {
        // executar a query
        $query->execute();

        // fechar a ligacao
        $this->BDligacao->close();

        // array erro e descricao
        $arrErro = array(
            "numeroTransacao" => $query->errno,
            "descricaoTransacao" => $query->error
        );

        // retornar booleano
        return $arrErro;
    }

    //METODO PARA APAGAR
    public function delete($query)
    {
        // executar a query
        $query->execute();

        // fechar a ligacao
        $this->BDligacao->close();

        // array erro e descricao
        $arrErro = array(
            "numeroTransacao" => $query->errno,
            "descricaoTransacao" => $query->error
        );

        // retornar booleano
        return $arrErro;
    }
    //METODO PARA OBTER DADOS
    public function selectRow($query)
    {
        // declarar array associativo de resultados
        $arrayResultados = array();
        // executar query
        $query->execute();

        // resultado
        $resultado = $query->get_result();

        while ($row = $resultado->fetch_row()) {
            // push para o arrayResultados
            array_push($arrayResultados, $row);
        }

        $query->free_result();

        // fechar ligacao
        $this->BDligacao->close();

        // adicionar chave de erros
        $arrayErros = array(
            "numeroTransacao" => $query->errno,
            "descricaoTransacao" => $query->error
        );

        // push dos erros para o arrayResultados
        array_push($arrayResultados, $arrayErros);

        // retornar array associativo
        return $arrayResultados;
    }

}