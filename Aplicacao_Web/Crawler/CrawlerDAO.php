<?php

require_once __DIR__ . '/BaseDAO.php';

class CrawlerDAO extends BaseDAO {

    //ATRIBUTOS
    //ACESSORES
    //CONSTRUTOR
    function __construct() {
        parent::__construct();
    }

    //METODOS
    //METODO PARA INSERIR
    public function insereWebsite($dominio, $ip) {
        //QUERY
        $query = "INSERT INTO website (dominio,ip) VALUES(?,?)";

        //PREPARA A QUERY
        $query = $this->prepare($query);

        //BIND
        $query->bind_param(
                'ss', $dominio, $ip);

        //EXECUTAR INSERT
        $insert = $this->insert($query);

        //FECHA CONEXAO
        $query->close();

        //RETORNA DADOS
        return $insert;
    }
    
    public function inserePagina($titulo, $imagem, $idWebsite, $url, $ultimaVisita, $ultimoEstado, $referer) {
        //QUERY
        $query = "INSERT INTO pagina (titulo,imagem,id_website,url,ultima_visita,ultimo_estado,referer) VALUES(?,?,?,?,?,?,?)";

        //PREPARA A QUERY
        $query = $this->prepare($query);

        //BIND
        $query->bind_param(
                'ssissss', $titulo, $imagem, $idWebsite,$url, $ultimaVisita, $ultimoEstado,$referer);

        //EXECUTAR INSERT
        $insert = $this->insert($query);

        //FECHA CONEXAO
        $query->close();

        //RETORNA DADOS
        return $insert;
    }

    public function obtemPaginas($idWebsite, $idPagina, $url) {
        //QUERY
        $query = "SELECT 
                    id_pagina,
                    titulo,
                    imagem,
                    id_website,
                    url,
                    ultima_visita,
                    ultimo_estado
                  FROM pagina
                  WHERE
                    (id_website = IFNULL(?, id_website) OR ? IS NULL) AND
                    (id_pagina = IFNULL(?, id_pagina) OR ? IS NULL) AND
                    (url = IFNULL(?, url) OR ? IS NULL)";

        //PREPARA A QUERY
        $query = $this->prepare($query);

        //BIND
        $query->bind_param(
                'iiiiss', $idWebsite,$idWebsite, $idPagina,$idPagina,$url,$url);

        //EXECUTAR SELECT
        $select = $this->select($query);

        //FECHA CONEXAO
        $query->close();

        //RETORNA DADOS
        return $select;
    }
    
    public function obtemWebsites($idWebsite, $dominio, $ip) {
        //QUERY
        $query = "SELECT 
                    id_website,
                    dominio,
                    ip
                  FROM website
                  WHERE
                    (id_website = IFNULL(?, id_website) OR ? IS NULL) AND
                    (dominio = IFNULL(?, dominio) OR ? IS NULL) AND
                    (ip = IFNULL(?, ip) OR ? IS NULL)";

        //PREPARA A QUERY
        $query = $this->prepare($query);

        //BIND
        $query->bind_param(
                'iissss', $idWebsite,$idWebsite,$dominio,$dominio,$ip,$ip);

        //EXECUTAR SELECT
        $select = $this->select($query);

        //FECHA CONEXAO
        $query->close();

        //RETORNA DADOS
        return $select;
    }
    
    public function removePagina($url) {
        //QUERY
        $query = "DELETE FROM pagina WHERE url = ?";

        //PREPARA A QUERY
        $query = $this->prepare($query);

        //BIND
        $query->bind_param(
                's', $url);

        //EXECUTAR DELETE
        $delete = $this->delete($query);

        //FECHA CONEXAO
        $query->close();

        //RETORNA DADOS
        return $delete;
    }

}
