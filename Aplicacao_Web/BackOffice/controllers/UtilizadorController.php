<?php

    require_once __DIR__.'/../dao/UtilizadorDAO.php';
    require_once __DIR__.'/../dto/UtilizadorDTO.php';

    require_once 'BaseController.php';


    class UtilizadorController extends BaseController
    {


//userlogin
        public function login($email,$password)
        {
            //INSTANCIAR DAO
            $userDAO = new UtilizadorDAO();
            //INSTANCIAR MODELO
            $userDTO = new UtilizadorDTO();
            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validaEmail($email),
                Validator::validaPreenchido($password),
                Validator::validarLogin($email, $password)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $userDTO
                        , $validacao);
                }
            }


            //OBTER USER
            $user = $userDAO->consultar(null,$email);
            $data_now=new DateTime();
            $data_token=DateTime::createFromFormat('Y-m-d H:i:s',$user[0]["data_token"]);
            $diff=$data_now->diff($data_token)->h;
            if ($diff >= 3){
                    //Criar token
                    $newToken = hash("sha256", $user[0]["email"] .$data_now->format("Y-m-d H:i:s"));
                    $userDAO->editar($user[0]["id_utilizador"],$newToken,$data_now->format("Y-m-d H:i:s"));
                    $userDTO->setToken($newToken);
                    $userDTO->setDataToken($data_now->format("Y-m-d H:i:s"));

                }else{
                    $userDTO->setToken($user[0]["token"]);
                    $userDTO->setDataToken($user[0]["data_token"]);
                }
            //SETAR MODELO
            $userDTO->setIdUtilizador($user[0]["id_utilizador"]);
            $userDTO->setEmail($user[0]["email"]);
            $userDTO->setDescricaoTransacao("Sucesso");
            $userDTO->setNumeroTransacao(0);


            //CONVERTER JSON
            return json_encode($userDTO->jsonSerialize(), JSON_UNESCAPED_UNICODE);
        }

        public function  autenticar($email,$token){
            
            //INSTANCIAR DAO
            $userDAO = new UtilizadorDAO();
            //INSTANCIAR MODELO
            $userDTO = new UtilizadorDTO();
            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validarSessao($email,$token)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $userDTO
                        , $validacao);
                }
            }
            //OBTER USER
            $user = $userDAO->consultar(null,$email);
            //SETAR MODELO
            $userDTO->setIdUtilizador($user[0]["id_utilizador"]);
            $userDTO->setEmail($user[0]["email"]);
            $userDTO->setToken($user[0]["token"]);
            $userDTO->setDataToken($user[0]["data_token"]);
            $userDTO->setDescricaoTransacao("Sucesso");
            $userDTO->setNumeroTransacao(0);

            //CONVERTER JSON
            return json_encode($userDTO->jsonSerialize(), JSON_UNESCAPED_UNICODE);

        }

    }


