<?php

require_once __DIR__ . "/../../Utils/Validator.php";
require_once __DIR__.'/../dao/UtilizadorDAO.php';
require_once __DIR__.'/../dto/UtilizadorDTO.php';
class BaseController
{

    //METODO PARA OBTER AS COLUNAS DE TRANSACAO DAS QUERIES
    public function obtemColunasTransacao($resultado) {
        $numeroTransacao = $this->obtemColuna($resultado, "numeroTransacao");

        $descricaoTransacao = $this->obtemColuna($resultado, "descricaoTransacao");

        $baseDTO = new BaseDTO();

        $baseDTO->setNumeroTransacao($numeroTransacao);
        $baseDTO->setDescricaoTransacao($descricaoTransacao);

        return $baseDTO;
    }

    //METODO PARA RETORNAR ERROS
    public function retornaErro($entidade, $erro) {
        $entidade->setNumeroTransacao($erro->getNumeroTransacao());
        $entidade->setDescricaoTransacao($erro->getDescricaoTransacao());

        return $entidade;
    }

    //METODO PARA RETORNAR ERROS EM JSON
    public function retornaErroJSON($entidade, $erro) {
        $entidade = $this->retornaErro($entidade, $erro);

        return json_encode($entidade->jsonSerialize(), JSON_UNESCAPED_UNICODE);
    }

    //METODO PARA RETORNAR UMA ENTIDADE EM JSON
    public function retornaEntidadeJSON($entidade) {
        return json_encode($entidade->jsonSerialize(), JSON_UNESCAPED_UNICODE);
    }

    //METODO PARA RETORNAR ARRAYS DE ENTIDADES EM JSON
    public function retornaArrayJSON($array) {
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    //METODO PARA OBTER COLUNAS DE ARRAYS
    public function obtemColuna($array, $coluna) {
        return isset($array[$coluna]) ? $array[$coluna] : NULL;
    }

    //METODO PARA REMOVER INDICES DE ARRAYS
    public function removeIndex(&$array, $indice) {
        unset($array[$indice]);
    }
    //converter  password
    public function criarHasher($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);

    }


}