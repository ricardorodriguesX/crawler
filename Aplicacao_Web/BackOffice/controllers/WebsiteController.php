<?php

require_once __DIR__ . "/../dao/WebsiteDAO.php";
require_once __DIR__ . "/../dto/WebsiteDTO.php";

require_once "BaseController.php";

class WebsiteController extends BaseController {

    public function submeterListaEntryPoints($email, $token, $lista) {
        //INSTANCIAR MODELO
        $websiteDTO = new WebsiteDTO();

        //VALIDA DADOS
        $arrayValidacoes = array(
            Validator::validarSessao($email, $token)
        );

        foreach ($arrayValidacoes as $validacao) {
            if ($validacao->getNumeroTransacao() < 0) {
                //RETORNA DTO VAZIO COM ERRO
                return $this->retornaErroJSON(
                                $websiteDTO
                                , $validacao);
            }
        }

        $arrayDados = array();
        
        if (is_array($lista)) {
            foreach ($lista as $link) {
                if($this->validateUrl($link)){
                    array_push($arrayDados, array("link"=>$link,"referer"=>$link));
                }
            }
        }
        
        $arrayDados = array(
            "links" => $arrayDados
        );
        
        $ficheiro = fopen(__DIR__."/../../Utils/entryPoints.json", "w");
        
        fwrite($ficheiro, json_encode($arrayDados));
    }

    public function obterListaEntryPoints($email, $token) {
        //INSTANCIAR MODELO
        $websiteDTO = new WebsiteDTO();

        //VALIDA DADOS
        $arrayValidacoes = array(
            Validator::validarSessao($email, $token)
        );

        foreach ($arrayValidacoes as $validacao) {
            if ($validacao->getNumeroTransacao() < 0) {
                //RETORNA DTO VAZIO COM ERRO
                return $this->retornaErroJSON(
                                $websiteDTO
                                , $validacao);
            }
        }

        return file_get_contents(__DIR__ . "/../../Utils/entryPoints.json");
    }

    public function obter($email, $token, $id_website, $requestData) {
        //INSTANCIAR DAO
        $websiteDAO = new WebsiteDAO();
        //INSTANCIAR MODELO
        $websiteDTO = new WebsiteDTO();

        //VALIDA DADOS
        $arrayValidacoes = array(
            Validator::validarSessao($email, $token)
        );

        foreach ($arrayValidacoes as $validacao) {
            if ($validacao->getNumeroTransacao() < 0) {
                //RETORNA DTO VAZIO COM ERRO
                return $this->retornaErroJSON(
                                $websiteDTO
                                , $validacao);
            }
        }
        $columns = array(
            // datatable column index  => database column name
            0 => 'dominio',
            1 => 'ip'
        );
        //OBTER USER
        $totalData = $websiteDAO->count_total($id_website, $requestData);
        $webs = $websiteDAO->consultarProcura($id_website, $requestData, $columns);
        unset($webs[sizeof($webs) - 1]);
        $totalFiltered = count($totalData);
        $data = array();
        foreach ($webs as $web) {
            $nestedData = array();
            $nestedData[] = $web['dominio'];
            $nestedData[] = $web['ip'];
            $nestedData[] =  '<a class="btn btn-xs btn-success admin" href="website?id='.$web['id_website'].'" target="_blank">Abrir</a>';
            $data[] = $nestedData;
        }

        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data   // total data array
        );

        return json_encode($json_data, JSON_UNESCAPED_UNICODE);  // send data as json format
    }

    public function contarPainel($email, $token) {
        //INSTANCIAR DAO
        $websiteDAO = new WebsiteDAO();
        //INSTANCIAR MODELO
        $websiteDTO = new WebsiteDAO();

        //VALIDA DADOS
        $arrayValidacoes = array(
            Validator::validarSessao($email, $token)
        );

        foreach ($arrayValidacoes as $validacao) {
            if ($validacao->getNumeroTransacao() < 0) {
                //RETORNA DTO VAZIO COM ERRO
                return $this->retornaErroJSON(
                                $websiteDTO
                                , $validacao);
            }
        }
        //OBTER USER
        $web = $websiteDAO->consultar();
        unset($web[sizeof($web) - 1]);
        $web = count($web);

        $json_data = array(
            "total" => intval($web),
            "numeroTransacao" => intval(0),
            "descricaoTransacao" => "Sucesso"
        );
        return json_encode($json_data, JSON_UNESCAPED_UNICODE);  // send data as json format
    }

    
    private function validateUrl($url) {
        $pattern = "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";

        return preg_match($pattern, $url);
    }


    public function obterWebsite($email, $token,$id){
        //INSTANCIAR DAO
        $websiteDAO = new WebsiteDAO();
        //INSTANCIAR MODELO
        $websiteDTO = new WebsiteDTO();

        //VALIDA DADOS
        $arrayValidacoes = array(
            Validator::validarSessao($email, $token)
        );

        foreach ($arrayValidacoes as $validacao) {
            if ($validacao->getNumeroTransacao() < 0) {
                //RETORNA DTO VAZIO COM ERRO
                return $this->retornaErroJSON(
                    $websiteDTO
                    , $validacao);
            }
        }
        //OBTER USER
        $web = $websiteDAO->consultarPainelWebsite($id);
        unset($web[sizeof($web) - 1]);

        $json_data = array(
            "dominio"=> $web[0]['dominio'],
            "ip" => $web[0]['ip'],
            "total_paginas"=> $web[0]['total_paginas'],
            "ultimo_crawler" => $web[0]['ultimo_crawler'],
            "numeroTransacao" => intval(0),
            "descricaoTransacao" => "Sucesso"
        );
        return json_encode($json_data, JSON_UNESCAPED_UNICODE);  // send data as json format
    }
}
