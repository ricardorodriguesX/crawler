<?php
    require_once __DIR__."/../dao/PaginaDAO.php";
    require_once __DIR__ . "/../dto/PaginaDTO.php";

    require_once "BaseController.php";


    class PaginaController extends BaseController
    {
//Obter user
        public function obter($email,$token,$id_pagina,$requestData){
            //INSTANCIAR DAO
            $paginaDAO = new PaginaDAO();
            //INSTANCIAR MODELO
            $paginaDTO = new PaginaDTO();

            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validarSessao($email,$token)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $paginaDTO
                        , $validacao);
                }
            }
            $columns = array(
                // datatable column index  => database column name
                0=> 'titulo',
                1=> 'dominio',
                2=>"url",
                3=>"ultima_visita",
                4=>"ultimo_estado"
            );
            //OBTER USER
            $totalData = $paginaDAO->count_total($id_pagina,$requestData);
            $pages=$paginaDAO->consultarProcura($id_pagina,$requestData,$columns);
            unset($pages[sizeof($pages)-1]);
            $totalFiltered= count($totalData);
            $data = array();
            foreach ($pages as $page){
                $nestedData=array();
                $nestedData[] = $page['titulo'];
                $nestedData[] = $page['dominio'];
                $nestedData[] = $page['url'];
                $nestedData[] = $page['ultima_visita'];
                $nestedData[] = $page['ultimo_estado'];
                $nestedData[] =  !empty($page['imagem']) && !is_null($page['imagem']) ? '<a class="btn btn-xs btn-success admin" href="../imagens/'.$page['imagem'].'" target="_blank"> <i class="fa fa-image"></i> Ver</a>' : "-";
                $data[] = $nestedData;
            }

            $json_data = array(
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $data   // total data array
            );

            return json_encode($json_data,JSON_UNESCAPED_UNICODE);  // send data as json format
        }



           public function chartPagina($email,$token){
            //INSTANCIAR DAO
            $paginaDAO = new PaginaDAO();
            //INSTANCIAR MODELO
            $paginaDTO = new PaginaDTO();

            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validarSessao($email,$token)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $paginaDTO
                        , $validacao);
                }
            }
            //OBTER USER
            $pages=$paginaDAO->chartPagina();
            unset($pages[sizeof($pages)-1]);
            $json_data = array(
                "jan" => intval($pages[0]['jan']),
                "feb" => intval($pages[0]['feb']),
                "mar" => intval($pages[0]['mar']),
                "abr" => intval($pages[0]['abr']),
                "mai" => intval( $pages[0]['mai']),
                "jun" => intval($pages[0]['jun']),
                "jul" => intval($pages[0]['jul']),
                "ago" => intval($pages[0]['ago']),
                "set" => intval($pages[0]['set']),
                "out" => intval($pages[0]['out']),
                "nov" => intval($pages[0]['nov']),
                "dez" => intval($pages[0]['dez']),
                "numeroTransacao"=>intval(0),
                "descricaoTransacao" => "Sucesso"
            );

            return json_encode($json_data,JSON_UNESCAPED_UNICODE);  // send data as json format
        }

        public function contarPainel($email,$token){
            //INSTANCIAR DAO
            $paginaDAO = new PaginaDAO();
            //INSTANCIAR MODELO
            $paginaDTO = new PaginaDTO();

            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validarSessao($email,$token)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $paginaDTO
                        , $validacao);
                }
            }
            //OBTER USER
            $pages=$paginaDAO->contarPainel();
            unset($pages[sizeof($pages)-1]);
            $json_data = array(
                "mes" => intval($pages[0]['mes']),
                "ano" => intval($pages[0]['ano']),
                "sem" => intval($pages[0]['sem']),
                "numeroTransacao"=>intval(0),
                "descricaoTransacao" => "Sucesso"
            );

            return json_encode($json_data,JSON_UNESCAPED_UNICODE);  // send data as json format
        }


        public function obterPaginasSite($email,$token,$id_website){
            //INSTANCIAR DAO
            $paginaDAO = new PaginaDAO();
            //INSTANCIAR MODELO
            $paginaDTO = new PaginaDTO();

            //VALIDA DADOS
            $arrayValidacoes = array(
                Validator::validarSessao($email,$token)
            );

            foreach ($arrayValidacoes as $validacao) {
                if ($validacao->getNumeroTransacao() < 0) {
                    //RETORNA DTO VAZIO COM ERRO
                    return $this->retornaErroJSON(
                        $paginaDTO
                        , $validacao);
                }
            }
            //OBTER USER
            $pages=$paginaDAO->consultarPaginasSite(null,$id_website);
            unset($pages[sizeof($pages)-1]);

            $data = array();
            foreach ($pages as $page){
                if($page['url']==$page['referer']){
                    array_push($data,array("name"=>substr($page['url'],0),"parent"=>null));
                }else {
                    array_push($data, array("name" => substr($page['url'],0), "parent" =>  substr($page['referer'],0)));
                }
            }
            return json_encode($data);  // send data as json format
        }



    }
 
 
 
 