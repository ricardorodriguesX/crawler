// Call the dataTables jQuery plugin
$(document).ready(function () {
    var tableWeb = $('#tableWeb').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "services/Website/Obter.php", // json datasource
            type: "post", // method  , by default get
            error: function () {  // error handling
                $(".employee-grid-error").html("");
                $("#tableWeb").append('<tbody class="employee-grid-error"><tr><th colspan="3">Nenhum dado encontrado no servidor</th></tr></tbody>');
            }
        }
    });
    var tablePag = $('#tablePag').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "services/Pagina/Obter.php", // json datasource
            type: "post", // method  , by default get
            error: function () {  // error handling
                $(".employee-grid-error").html("");
                $("#tableWeb").append('<tbody class="employee-grid-error"><tr><th colspan="6">Nenhum dado encontrado no servidor</th></tr></tbody>');
            }
        }
    });
    
    var obj = new Array();
    $.ajax({
        type: "POST",
        url: "services/Pagina/ContarPainel.php",
        async: true,
    }).done(function (resposta) {
        obj = JSON.parse(resposta);
        $("#totalPaginas").text(obj.ano);
        $("#paginasEsteMes").text(obj.mes);
        $("#semanaPaginas").text(obj.sem);
        if (obj.numeroTransacao < 0) {
            window.location.replace('index');
        }
    }).fail(function (resposta) {
        console.log(resposta);
    });
    var obj = new Array();
    $.ajax({
        type: "POST",
        url: "services/Website/ContarPainel.php",
        async: true,
    }).done(function (resposta) {
        obj = JSON.parse(resposta);
        $("#totalDominos").text(obj.total);
        if (obj.numeroTransacao < 0) {
            window.location.replace('index');
        }
    }).fail(function (resposta) {
        console.log(resposta);
    });
    
    $.ajax({
        type: "POST",
        url: "services/Website/ObterEntryPointsService.php",
        async: true,
    }).done(function (resposta) {
        var obj = JSON.parse(resposta).links;
        
        for (var link of obj) {
            var row = `<li><input class='input-link mb-3 form-input w-100' value='` + link.link + `'></li>`;
            $("#listaEntryPoints").append(row);
        }
        
        handlerInput();
        handlerSubmit();
    }).fail(function (resposta) {
        console.log(resposta);
    });
    
    
    $("#adicionaLink").click(function () {
        var row = `<li><input class='input-link mb-3 form-input w-100'></li>`;
        $("#listaEntryPoints").append(row);
        handlerInput();
        handlerSubmit();
    });
    
    
});

function handlerInput() {
    $(".input-link").change(function (evento) {
        evento.stopPropagation();
        evento.stopImmediatePropagation();
        
        if ($(this).val().length <= 0) {
            $(this).parent().remove();
        }
    });
}

function handlerSubmit() {
    $("#submeterLinks").unbind().click(function () {
        var links = $("#listaEntryPoints li");
        
        var array = [];
        for (var link of links) {
            var elemento = $(link).find("input");
            
            if (elemento.val().length > 0) {
                array.push(elemento.val());
            }
        }
        $.ajax({
            type: "POST",
            url: "services/Website/SubmeterEntryPointsService.php",
            async: true,
            data: {
                lista: JSON.stringify(array)
            }
        });
    });
}