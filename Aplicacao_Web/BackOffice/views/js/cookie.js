function setCookie(cname,cvalue,minutes) {
    var d = new Date();
    d.setTime(d.getTime() + (minutes*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}
function createCookie(time,email,token){
    setCookie('email',email,time);
    setCookie('token',token,time);
}
function createSession(email,token) {
    createCookie(180,email,token);
}
function deleteCookieSessions() {
    createCookie(-1800,'','','');
} 