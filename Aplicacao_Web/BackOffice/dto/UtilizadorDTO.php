<?php
    require_once "BaseDTO.php";
class UtilizadorDTO extends BaseDTO
{
    private $id_utilizador;
    private $email;
    private $password;
    private $token;
    private $data_token;

    //construtor
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getIdUtilizador()
    {
        return $this->id_utilizador;
    }

    /**
     * @param mixed $id_utilizador
     */
    public function setIdUtilizador($id_utilizador)
    {
        $this->id_utilizador = $id_utilizador;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDataToken()
    {
        return $this->data_token;
    }

    /**
     * @param mixed $data_token
     */
    public function setDataToken($data_token)
    {
        $this->data_token = $data_token;
    }

//METODOS
        public function jsonSerialize() {
            return [
                "id_utilizador"=>$this->id_utilizador,
                "email" => $this->email,
                "token"=>$this->token,
                "data_token"=> $this->data_token,
                "descricaoTransacao"=>$this->getDescricaoTransacao(),
                "numeroTransacao" => $this->getNumeroTransacao()
            ];
        }
}

