<?php

    require_once __DIR__."/BaseDTO.php";

    class WebsiteDTO extends BaseDTO
    {

        private $ip;
        private $dominio;
        private $id_website;



        //construtor
        public function __construct()
        {
        }

        public function getIp()
        {
            return $this->ip;
        }

        public function setIp($ip)
        {
            $this->ip= $ip;
        }
        public function getDominio()
        {
            return $this->dominio;
        }

        public function setDominio($dominio)
        {
            $this->dominio= $dominio;
        }
        public function getId()
        {
            return $this->id_website;
        }

        public function setId($id)
        {
            $this->id_website= $id;
        }



//METODOS
        public function jsonSerialize()
        {
            return [
                "ip" => $this->getIp(),
                "dominio" => $this->getDominio(),
                "id_website" => $this->getId(),

                "descricaoTransacao" => $this->getDescricaoTransacao(),
                "numeroTransacao" => $this->getNumeroTransacao()
            ];
        }
    }
