<?php


    class BaseDTO implements JsonSerializable
    {
        //atributos
        private $numeroTransacao;
        private $descricaoTransacao;

        //construtores
        public function __construct()
        {

        }


        //gets
        /**
         * @return mixed
         */
        public function getNumeroTransacao()
        {
            return $this->numeroTransacao;
        }

        /**
         * @param mixed $numeroTransacao
         */
        public function setNumeroTransacao($numeroTransacao): void
        {
            $this->numeroTransacao = $numeroTransacao;
        }

        /**
         * @return mixed
         */
        public function getDescricaoTransacao()
        {
            return $this->descricaoTransacao;
        }

        /**
         * @param mixed $descricaoTransacao
         */
        public function setDescricaoTransacao($descricaoTransacao): void
        {
            $this->descricaoTransacao = $descricaoTransacao;
        }


//METODOS
        public function jsonSerialize() {
            return [
                "numeroTransacao"=>$this->numeroTransacao,
                "descricaoTransacao" => $this->descricaoTransacao
            ];
        }


    }