<?php

    require_once __DIR__."/BaseDTO.php";

    class PaginaDTO extends BaseDTO
    {

        private $titulo;
        private $url;
        private $id_pagina;

        private $imagem;
        private $id_website;
        private $ultima_visita;
        private $ultimo_estado;

//construtor
        public function __construct()
        {
        }

        public function getTitulo()
        {
            return $this->titulo;
        }

        public function setTitulo($titulo)
        {
            $this->titulo= $titulo;
        }

        public function getUrl()
        {
            return $this->url;
        }

        public function setUrl($url)
        {
            $this->url= $url;
        }

        public function getId()
        {
            return $this->id_pagina;
        }

        public function setId($id_pagina)
        {
            $this->id_pagina= $id_pagina;
        }

        public function getImagem()
        {
            return $this->imagem;
        }

        public function setImagem($imagem)
        {
            $this->imagem= $imagem;
        }
        public function getId_website()
        {
            return $this->id_website;
        }

        public function setId_website($id_website)
        {
            $this->id_website= $id_website;
        }
        public function getUltima_visita()
        {
            return $this->ultima_visita;
        }

        public function setUltima_visita($ultima_visita)
        {
            $this->ultima_visita= $ultima_visita;
        }
        public function getUltimo_estado()
        {
            return $this->ultimo_estado;
        }

        public function setUltimo_estado($ultimo_estado)
        {
            $this->ultimo_estado= $ultimo_estado;
        }



//METODOS
        public function jsonSerialize()
        {
            return [
                "titulo" => $this->getTitulo(),
                "url" => $this->getUrl(),
                "id_pagina" => $this->getId(),
                "imagem" => $this->getImagem(),
                "id_website" => $this->getId_website(),
                "ultima_visita" => $this->getUltima_visita(),
                "ultimo_estado" => $this->getUltimo_estado(),
                "descricaoTransacao" => $this->getDescricaoTransacao(),
                "numeroTransacao" => $this->getNumeroTransacao()
            ];
        }
    }
