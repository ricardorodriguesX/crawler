<?php
    require_once __DIR__ . "/../../controllers/PaginaController.php";
    //POSTS
    $email = !empty($_COOKIE["email"]) && isset($_COOKIE["email"]) ? filter_input(INPUT_COOKIE,"email",FILTER_SANITIZE_EMAIL) : NULL;
    $token = !empty($_COOKIE["token"]) && isset($_COOKIE["token"]) ? filter_input(INPUT_COOKIE,"token",FILTER_SANITIZE_STRING): NULL;


    //INSTANCIAR CONTROLADOR
    $controlador = new PaginaController();
    //INSERIR
    $resposta = $controlador->contarPainel($email,$token);
    //RETORNAR RESPOSTA
    echo $resposta;
    
    