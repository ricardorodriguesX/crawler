<?php
    require_once __DIR__."/../../controllers/PaginaController.php";
    //POSTS
    $email = !empty($_COOKIE["email"]) && isset($_COOKIE["email"]) ? filter_input(INPUT_COOKIE,"email",FILTER_SANITIZE_EMAIL) : NULL;
    $token = !empty($_COOKIE["token"]) && isset($_COOKIE["token"]) ? filter_input(INPUT_COOKIE,"token",FILTER_SANITIZE_STRING): NULL;
    $id_pagina = !empty($_POST["id_pagina"]) && isset($_POST["id_pagina"]) ? filter_input(INPUT_POST,"id_pagina",FILTER_SANITIZE_NUMBER_INT): NULL;

    //INSTANCIAR CONTROLADOR
    $controlador = new PaginaController();
    //INSERIR
    $resposta = $controlador->obter($email,$token,$id_pagina,$_REQUEST);
    //RETORNAR RESPOSTA
    echo $resposta;
    
    