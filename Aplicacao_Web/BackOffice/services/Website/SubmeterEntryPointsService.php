<?php

require_once __DIR__ . "/../../controllers/WebsiteController.php";

//POSTS
$email = !empty($_COOKIE["email"]) && isset($_COOKIE["email"]) ? filter_input(INPUT_COOKIE, "email", FILTER_SANITIZE_EMAIL) : NULL;
$token = !empty($_COOKIE["token"]) && isset($_COOKIE["token"]) ? filter_input(INPUT_COOKIE, "token", FILTER_SANITIZE_STRING) : NULL;
$lista = isset($_POST["lista"]) &&  !empty($_POST["lista"]) ? json_decode(html_entity_decode(stripslashes($_POST["lista"])),true) : NULL;
$lista = is_null($lista) ? array() : $lista;

//INSTANCIAR CONTROLADOR
$controlador = new WebsiteController();
//INSERIR
$resposta = $controlador->submeterListaEntryPoints($email, $token, $lista);
//RETORNAR RESPOSTA
echo $resposta;

