<?php
require_once __DIR__ . "/../../controllers/WebsiteController.php";
//POSTS
$email = !empty($_COOKIE["email"]) && isset($_COOKIE["email"]) ? filter_input(INPUT_COOKIE, "email", FILTER_SANITIZE_EMAIL) : NULL;
$token = !empty($_COOKIE["token"]) && isset($_COOKIE["token"]) ? filter_input(INPUT_COOKIE, "token", FILTER_SANITIZE_STRING) : NULL;

$id = !empty($_POST["id"]) && isset($_POST["id"]) ? $_POST["id"] : NULL;
//INSTANCIAR CONTROLADOR
$controlador = new WebsiteController();
//INSERIR
echo $controlador->obterWebsite($email, $token,$id);
