<?php
    require_once __DIR__ . '/../../controllers/UtilizadorController.php';

//POSTS
     $password = !empty($_POST["password"]) && isset($_POST["password"]) ?  filter_input(INPUT_POST,"password",FILTER_SANITIZE_STRING) : NULL;
     $email = !empty($_POST["email"]) && isset($_POST["email"]) ?  filter_input(INPUT_POST,"email",FILTER_SANITIZE_EMAIL) : NULL;

//INSTANCIAR CONTROLADOR
    $controlador = new UtilizadorController();
//INSERIR
    $resposta = $controlador->login($email,$password);
//RETORNAR RESPOSTA
   echo $resposta;
