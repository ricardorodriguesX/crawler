
<?php

require_once __DIR__."/BaseDAO.php";

class WebsiteDAO extends BaseDAO
{
    //METODOS
    public function consultar($id_website=null,$ip=null, $dominio=null ){
    
        //PREPARAR QUERY
        $stmt = $this->prepare("SELECT * FROM website
            WHERE 
            (ip= IFNULL(?, ip) OR ? IS NULL)  AND
            (dominio= IFNULL(?, dominio) OR ? IS NULL)  AND
            (id_website= IFNULL(?, id_website) OR ? IS NULL) ");

        //BIND
        $stmt->bind_param("ssssii", $ip,$ip,$dominio,$dominio,$id_website,$id_website);

        //EXECUTAR COM CONTROLO
        $controlo = $this->select($stmt);


        //FECHAR STATMENT
        $stmt->close();

        //RETORNAR CONTROLO
        return $controlo;
    }
    public function count_total($id_website,$requestData)
    {
        $sql ="SELECT * FROM website ";
        if(!empty($requestData['search']['value']) || $id_website != null){
            $sql.=" where id_website='".$id_website."%' ";
            $sql.=" OR ip Like '".$requestData['search']['value']."%' ";
            $sql.=" OR dominio Like '".$requestData['search']['value']."%' ";
        }

        $stmt = $this->prepare($sql);
        //EXECUTAR COM CONTROLO
        $controlo = $this->select($stmt);

        //FECHAR STATMENT
        $stmt->close();
        //RETORNAR CONTROLO
        return $controlo;
    }

    public function consultarProcura($id_website,$requestData,$columns){
        //PREPARAR QUERY
        $sql ="SELECT * FROM website ";
        if(!empty($requestData['search']['value']) || $id_website != null){
            $sql.=" where id_website='".$id_website."%' ";
            $sql.=" OR ip Like '".$requestData['search']['value']."%' ";
            $sql.=" OR dominio Like '".$requestData['search']['value']."%' ";
        }

        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".
            $requestData['start']."  ,".$requestData['length']."  ";

        $stmt = $this->prepare($sql);
        //EXECUTAR COM CONTROLO
        $controlo = $this->select($stmt);
        //FECHAR STATMENT
        $stmt->close();
        //RETORNAR CONTROLO
        return $controlo;
    }

    //METODOS
    public function consultarPainelWebsite($id_website=null){
        //PREPARAR QUERY
        $stmt = $this->prepare("SELECT dominio, ip, (SELECT count(id_pagina) from pagina where pagina.id_website=website.id_website) as total_paginas, (SELECT ultima_visita from pagina where pagina.id_website=website.id_website ORDER by ultima_visita DESC LIMIT 1 ) as ultimo_crawler FROM website  where id_website=? ");
        //BIND
        $stmt->bind_param("i", $id_website);
        //EXECUTAR COM CONTROLO
        $controlo = $this->select($stmt);
        //FECHAR STATMENT
        $stmt->close();
        //RETORNAR CONTROLO
        return $controlo;
    }

}
    