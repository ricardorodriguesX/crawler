<?php

require_once __DIR__."/BaseDAO.php";

class UtilizadorDAO extends BaseDAO
{

    //METODOS
    public function consultar($id_utilizador=null,$email=null,$password=null,$token=null,$data_token=null){
        //PREPARAR QUERY
        $stmt = $this->prepare("Select * FROM utilizador
            WHERE     (id_utilizador = IFNULL(?, id_utilizador) OR ? IS NULL) AND
                (email = IFNULL(?, email) OR ? IS NULL) AND 
                (password = IFNULL(?, password ) or ? IS NULL) AND
                 (token = IFNULL(?, token ) or ? IS NULL) AND
                (data_token = IFNULL(?, data_token ) or ? IS NULL)");

        //BIND
        $stmt->bind_param("iissssssss", $id_utilizador,$id_utilizador,$email,$email,$password,$password,$token,$token,$data_token,$data_token);
        //EXECUTAR COM CONTROLO
        $controlo = $this->select($stmt);

        //FECHAR STATMENT
        $stmt->close();
        //RETORNAR CONTROLO
        return $controlo;
    }

    public function editar ($id_utilizador,$token,$data_token){
        //PREPARAR QUERY
        $stmt = $this->prepare("UPDATE utilizador SET 
            token = IFNULL(?, token ),
            data_token = IFNULL(?, data_token )
            where id_utilizador = ?");
        //BIND
        $stmt->bind_param("ssi", $token,$data_token, $id_utilizador);
        //EXECUTAR COM CONTROLO
        $controlo = $this->update($stmt);

        //FECHAR STATMENT
        $stmt->close();
        //RETORNAR CONTROLO
        return $controlo;
    }


}