<?php

    require_once __DIR__."/BaseDAO.php";

    class PaginaDAO extends BaseDAO
    {


        public function count_total($id_pagina,$requestData)
        {
            $sql ="SELECT p.titulo,p.imagem ,w.dominio, p.url,p.ultima_visita, p.ultimo_estado FROM pagina p INNER JOIN website w ON w.id_website=p.id_website";
            if(!empty($requestData['search']['value']) || $id_pagina != null){
                $sql.=" where p.id_pagina='".$id_pagina."%' ";
                $sql.=" OR p.titulo Like '".$requestData['search']['value']."%' ";
                $sql.=" OR w.dominio Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.url Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.ultima_visita Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.ultimo_estado Like '".$requestData['search']['value']."%' ";
            }

            $stmt = $this->prepare($sql);
            //EXECUTAR COM CONTROLO
            $controlo = $this->select($stmt);

            //FECHAR STATMENT
            $stmt->close();
            //RETORNAR CONTROLO
            return $controlo;
        }

        public function consultarProcura($id_pagina,$requestData,$columns){
            //PREPARAR QUERY
            $sql ="SELECT p.titulo,p.imagem ,w.dominio, p.url,p.ultima_visita, p.ultimo_estado FROM pagina p INNER JOIN website w ON w.id_website=p.id_website";
            if(!empty($requestData['search']['value']) || $id_pagina != null){
                $sql.=" where p.id_pagina='".$id_pagina."%' ";
                $sql.=" OR p.titulo Like '".$requestData['search']['value']."%' ";
                $sql.=" OR w.dominio Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.url Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.ultima_visita Like '".$requestData['search']['value']."%' ";
                $sql.=" OR p.ultimo_estado Like '".$requestData['search']['value']."%' ";
            }

            $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".
                $requestData['start']."  ,".$requestData['length']."  ";

            $stmt = $this->prepare($sql);
            //EXECUTAR COM CONTROLO
            $controlo = $this->select($stmt);
            //FECHAR STATMENT
            $stmt->close();
            //RETORNAR CONTROLO
            return $controlo;
        }

        public function chartPagina(){
            $stmt = $this->prepare("SELECT (SELECT COUNT(url) FROM pagina WHERE MONTH(ultima_visita)=1 AND  YEAR(ultima_visita)=  Year(now())) as 'jan' ,
      (SELECT COUNT(url) FROM pagina WHERE MONTH(ultima_visita)=2 AND  YEAR(ultima_visita)=  Year(now())) as 'feb',
      (SELECT COUNT(url) FROM pagina WHERE MONTH(ultima_visita)=3 AND  YEAR(ultima_visita)=  Year(now())) as 'mar', 
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=4 AND  YEAR(ultima_visita)=  Year(now())) as 'abr' ,
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=5 AND  YEAR(ultima_visita)=  Year(now())) as 'mai',
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=6 AND  YEAR(ultima_visita)=  Year(now())) as 'jun',
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=7 AND  YEAR(ultima_visita)=  Year(now())) as 'jul' ,
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=8 AND  YEAR(ultima_visita)=  Year(now())) as 'ago' ,
      (SELECT COUNT(url) FROM pagina WHERE MONTH(ultima_visita)=9 AND  YEAR(ultima_visita)=  Year(now())) as 'set',
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=10 AND  YEAR(ultima_visita)=  Year(now())) as 'out', 
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=11 AND  YEAR(ultima_visita)=  Year(now())) as 'nov' ,
      (SELECT COUNT(url)  FROM pagina WHERE MONTH(ultima_visita)=12 AND  YEAR(ultima_visita)=  Year(now())) as 'dez'");

            //EXECUTAR COM CONTROLO
            $controlo = $this->select($stmt);
            //FECHAR STATMENT
            $stmt->close();
            //RETORNAR CONTROLO
            return $controlo;

        }
        public function contarPainel(){
            $stmt = $this->prepare("SELECT (SELECT COUNT(url) FROM pagina WHERE MONTH(ultima_visita)=MONTH(now()) AND YEAR(ultima_visita)=Year(now())) as 'mes',
 (SELECT COUNT(url) FROM pagina WHERE YEAR(ultima_visita)=Year(now())) as 'ano' , 
(SELECT COUNT(url) FROM pagina WHERE ultima_visita BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) AND NOW() ) as 'sem' ");

            //EXECUTAR COM CONTROLO
            $controlo = $this->select($stmt);
            //FECHAR STATMENT
            $stmt->close();
            //RETORNAR CONTROLO
            return $controlo;
        }

        //METODOS
        public function consultarPaginasSite($id_pagina=null,$id_website=null){
            //PREPARAR QUERY
            $stmt = $this->prepare("Select * FROM pagina
            WHERE     (id_pagina = IFNULL(?, id_pagina) OR ? IS NULL) AND
                (id_website = IFNULL(?, id_website) OR ? IS NULL)");

            //BIND
            $stmt->bind_param("iiii", $id_pagina,$id_pagina,$id_website,$id_website);
            //EXECUTAR COM CONTROLO
            $controlo = $this->select($stmt);

            //FECHAR STATMENT
            $stmt->close();
            //RETORNAR CONTROLO
            return $controlo;
        }





    }
    