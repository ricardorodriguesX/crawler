<?php
class Router {

    //ATRIBUTOS
    private $request;
    private $caminhoARemover;
    private $basePath;

    //ACESSORES
    public function getRequest() {
        return $this->request;
    }

    public function setResquest($request) {
        $this->request = $request;
    }

    public function getCaminhoARemover() {
        return $this->caminhoARemover;
    }

    public function setCaminhoServer($caminhoARemover) {
        $this->caminhoARemover = $caminhoARemover;
    }

    public function getBasePath() {
        return $this->basePath;
    }

    public function setBasePath($basePath) {
        $this->basePath = $basePath;
    }

    //CONSTRUTOR
    public function __construct($request = "", $caminhoARemover = "", $basePath = "") {
        //ADICIONA O ESTADO
        $this->request = $request;
        $this->caminhoARemover = $caminhoARemover;
        $this->basePath = $basePath;
    }

    //METODOS
    public function obtemPagina() {
        //PAGINA REQUERIDA
        $paginaRequerida = "$this->basePath/404.html";

        //REMOVE URL ABS
        $caminho = explode($this->caminhoARemover, $this->request)[1];
       
        //OBTEM ARRAY DE CAMINHOS POSSIVEIS
        $arrayCaminho = $this->multiExplode($caminho, array("/", "?"));
        
        //OBTEM PRIMEIRA PAGINA E MELHORA VISUAL PAGINA
        $pagina = str_replace("-", "_", $arrayCaminho[0]);
        
        //OBTEM PAGINA

        if ($pagina == "") {
            $paginaRequerida = "$this->basePath/index.html";
        } elseif (file_exists("$this->basePath/$pagina.html")) {
            $paginaRequerida ="$this->basePath/$pagina.html";
        } elseif (file_exists("$this->basePath/$caminho")) {
            $paginaRequerida = "$this->basePath/$caminho";
        }

        //RETORNA PAGINA REQUERIDA
        return $paginaRequerida;
    }

    //METODOS AUXILIARES
    private function multiExplode($string, $delimitadores) {
        $retorno = [];

        foreach ($delimitadores as $delimitador) {
            $retorno = explode($delimitador, $string);
        }

        return $retorno;
    }

}
